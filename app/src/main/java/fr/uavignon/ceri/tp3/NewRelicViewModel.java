package fr.uavignon.ceri.tp3;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import fr.uavignon.ceri.tp3.data.Relic;
import fr.uavignon.ceri.tp3.data.RelicRepository;

public class NewRelicViewModel extends AndroidViewModel {
    public static final String TAG = DetailViewModel.class.getSimpleName();

    private RelicRepository repository;
    private MutableLiveData<Relic> relic;

    public NewRelicViewModel(Application application) {
        super(application);
        repository = RelicRepository.get(application);
        relic = new MutableLiveData<>();
    }

    public void setRelic(String id) {
        repository.getRelic(id);
        relic = repository.getSelectedRelic();
    }

    LiveData<Relic> getRelic() {
        return relic;
    }

    public long insertOrUpdateRelic(Relic newRelic) {
        long res = 0;
        if (relic.getValue() == null) {
            res = repository.insertRelic(newRelic);
            // return -1 if there is a conflict
            //setRelic(res);
        } else {
            // ID does not change for updates
            newRelic.setId(relic.getValue().getId());
            int nb = repository.updateRelic(newRelic);
            // return the nb of rows updated by the query
            if (nb ==0)
                res = -1;
        }
        Log.d(TAG,"insert relic="+relic.getValue());
        return res;
    }
}
