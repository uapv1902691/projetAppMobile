package fr.uavignon.ceri.tp3.data.webservice;

import java.util.Map;

import fr.uavignon.ceri.tp3.data.RelicResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface OWMInterface {

    @GET("collection")
    public Call<Map<String,RelicResponse>> getListRelic();
    @GET("items/")
    public Call<RelicResponse> getDBRelic(@Query("q") String itemid);


}
