package fr.uavignon.ceri.tp3.data.database;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import fr.uavignon.ceri.tp3.data.Relic;

@Database(entities = {Relic.class}, version = 1, exportSchema = false)
public abstract class CeriMuseumDatabase extends RoomDatabase {

    private static final String TAG = CeriMuseumDatabase.class.getSimpleName();

    public abstract CeriMuseumDao ceriMuseumDao();

    private static CeriMuseumDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 1;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);


    public static CeriMuseumDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (CeriMuseumDatabase.class) {
                if (INSTANCE == null) {
                    // Create database here
                            // without populate
                        /*
                    INSTANCE =
                            Room.databaseBuilder(context.getApplicationContext(),
                                    WeatherRoomDatabase.class,"book_database")
                                    .build();

                     */

                            // with populate
                            INSTANCE =
                            Room.databaseBuilder(context.getApplicationContext(),
                                    CeriMuseumDatabase.class,"book_database")
                                    .addCallback(sRoomDatabaseCallback)
                                    .build();

                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback(){

                @Override
                public void onOpen (@NonNull SupportSQLiteDatabase db){
                    super.onOpen(db);

                    databaseWriteExecutor.execute(() -> {
                        // Populate the database in the background.
                        CeriMuseumDao dao = INSTANCE.ceriMuseumDao();
                        dao.deleteAll();

                        Relic[] relics = {};

                        for(Relic newRelic : relics)
                            dao.insert(newRelic);
                        Log.d(TAG,"database populated");
                    });

                }
            };



}
