package fr.uavignon.ceri.tp3.data;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Entity(tableName = "relic_table")
public class Relic {

    public static final long ADD_ID = -1;

    @PrimaryKey()
    @NonNull
    @ColumnInfo(name="_id")
    private String id;

    @ColumnInfo(name="pictures")
    @TypeConverters(HashMapTypeConverter.class)
    private Map<String, String> pictures;

    @ColumnInfo(name="year")
    private Integer year;

    @ColumnInfo(name="timeFrame")
    @TypeConverters(ListTypeConverter.class)
    private List<String> timeFrame;

    @ColumnInfo(name="name")
    private String name;

    @ColumnInfo(name="brand")
    private String brand;

    @ColumnInfo(name="description")
    private String description;

    @ColumnInfo(name="technicalDetails")
    @TypeConverters(ListTypeConverter.class)
    private List<String> technicalDetails;

    @ColumnInfo(name="categories")
    @TypeConverters(ListTypeConverter.class)
    private List<String> categories;

    @ColumnInfo(name="working")
    private Boolean working;

    @Ignore
    public Relic(@NonNull String id, HashMap<String, String> pictures, Integer year, List<String> timeFrame, String name, String brand, String description, List<String> technicalDetails, List<String> categories ,Boolean working) {

        this.id = id;
        this.pictures = pictures;
        this.year = year;
        this.timeFrame = timeFrame;
        this.name = name;
        this.brand = brand;
        this.description = description;
        this.technicalDetails = technicalDetails;
        this.categories = categories;
        this.working = working;

    }

    public Relic(String name,int year,String description) {
        this.name = name;
        this.description = description;
        this.year = year;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    @NonNull
    public Map<String, String> getPictures() {
        return pictures;
    }

    public void setPictures(Map<String, String> pictures) {
        this.pictures = pictures;
    }

    @NonNull
    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    @NonNull
    public List<String> getTimeFrame() {
        return timeFrame;
    }

    public void setTimeFrame(List<String> timeFrame) {
        this.timeFrame = timeFrame;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @NonNull
    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    @NonNull
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @NonNull
    public List<String> getTechnicalDetails() {
        return technicalDetails;
    }

    public void setTechnicalDetails(List<String> technicalDetails) {
        this.technicalDetails = technicalDetails;
    }

    @NonNull
    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    @NonNull
    public Boolean getWorking() {
        return working;
    }

    public void setWorking(Boolean working) {
        this.working = working;
    }
}
