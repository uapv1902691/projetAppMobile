package fr.uavignon.ceri.tp3;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.bumptech.glide.Glide;

import java.util.Set;

import fr.uavignon.ceri.tp3.data.Relic;

public class DetailFragment extends Fragment {
    public static final String TAG = DetailFragment.class.getSimpleName();

    private DetailViewModel viewModel;
    private TextView textRelicName, textBrand, textDescription, textTimeFrame, textCloudiness, textWind, textLastUpdate, textCategory, textYear, textTechnicalDetails, textWorking;
    private ImageView imgWeather;
    private ProgressBar progress;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);

        // Get selected city
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        String RelicID = args.getCityNum();
        Log.d(TAG,"selected id="+RelicID);
        viewModel.setRelic(RelicID);

        listenerSetup();
        observerSetup();

    }


    private void listenerSetup() {
        textRelicName = getView().findViewById(R.id.nameName);
        textBrand = getView().findViewById(R.id.nameBrand);
        textDescription = getView().findViewById(R.id.textDescription);
        textTimeFrame = getView().findViewById(R.id.textTimeFrame);
        textCloudiness = getView().findViewById(R.id.textYear);
        textWind = getView().findViewById(R.id.textCategories);
        textLastUpdate = getView().findViewById(R.id.textTechnicalDetails);

        imgWeather = getView().findViewById(R.id.imageRelic);

        progress = getView().findViewById(R.id.progress);

        getView().findViewById(R.id.buttonUpdate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewModel.updateRelic();
            }
        });

        getView().findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(fr.uavignon.ceri.tp3.DetailFragment.this)
                        .navigate(R.id.action_DetailFragment_to_ListFragment);
            }
        });
    }

    private void observerSetup() {
        viewModel.getRelic().observe(getViewLifecycleOwner(),
                relic -> {
                    if (relic != null) {
                        Log.d(TAG, "observing relic view");

                        textRelicName.setText(relic.getName());
                        textBrand.setText(relic.getBrand());
                        textDescription.setText(relic.getDescription());
                        textTimeFrame.setText(relic.getTimeFrame().toString());
                        textCategory.setText(relic.getCategories().toString());
                        textYear.setText(String.valueOf(relic.getYear()));
                        textTechnicalDetails.setText(relic.getTechnicalDetails().toString());
                        textWorking.setText(relic.getWorking().toString());



                        if (relic.getPictures() != null) {
                            Set<String> kk = relic.getPictures().keySet();
                            String key = null;
                            for (String mem : kk) {
                                key = mem;
                                break;
                            }
                            loadImage(imgWeather, "https://demo-lia.univ-avignon.fr/cerimuseum/items/" + relic.getId() + "/images/" + key,relic);
                            //https: //demo-lia.univ-avignon.fr/cerimuseum/items/ry8/images/IMG_1593
                            // /items/<itemID>/images/<imageID>
                        }
                    }else{
                        loadImage(imgWeather, "https://demo-lia.univ-avignon.fr/cerimuseum/items/"+relic.getId()+"/thumbnail",relic);
                    }

                });

                }

    public void loadImage(ImageView view, String url, Relic i) {

        if (url != null) {
            Glide.with(view.getContext())
                    .load(url)
                    .error(Glide.with(view).load("https://demo-lia.univ-avignon.fr/cerimuseum/items/"+i.getId()+"/thumbnail"))
                    .into(view);
        }

    }


}