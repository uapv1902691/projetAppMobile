package fr.uavignon.ceri.tp3.data;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import fr.uavignon.ceri.tp3.data.database.CeriMuseumDao;
import fr.uavignon.ceri.tp3.data.database.CeriMuseumDatabase;
import fr.uavignon.ceri.tp3.data.webservice.OWMInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

import static fr.uavignon.ceri.tp3.data.database.CeriMuseumDatabase.databaseWriteExecutor;

public class RelicRepository {

    private static final String TAG = RelicRepository.class.getSimpleName();

    private LiveData<List<Relic>> allRelics;
    private MutableLiveData<Relic> selectedRelic;

    private CeriMuseumDao ceriMuseumDao;
    private volatile MutableLiveData<Boolean> isLoading;
    private MutableLiveData<Throwable> webServiceThrowable;
    private volatile int nbAPIloads=0;
    private static int version = 0;
    private static volatile RelicRepository INSTANCE;
    private final OWMInterface api;
    private List<Relic> relicListfixed;

    public synchronized static RelicRepository get(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new RelicRepository(application);
        }

        return INSTANCE;
    }

    public RelicRepository(Application application) {

        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://demo-lia.univ-avignon.fr/cerimuseum/").addConverterFactory(MoshiConverterFactory.create()).build();

        api = retrofit.create(OWMInterface.class);

        CeriMuseumDatabase db = CeriMuseumDatabase.getDatabase(application);
        ceriMuseumDao = db.ceriMuseumDao();
        allRelics = ceriMuseumDao.getAllRelics();
        selectedRelic = new MutableLiveData<>();
        isLoading = new MutableLiveData<>();
        isLoading.postValue(Boolean.FALSE);
        webServiceThrowable = new MutableLiveData<>();

        //getVer();
        //if(version!=3){
            Log.d("MWEIN","updating collection");
            loadAllRelics();
        //}
        relicListfixed = ceriMuseumDao.getSynchrAllRelics();

    }

    public LiveData<List<Relic>> getAllRelics() {
        return allRelics;
    }

    public MutableLiveData<Relic> getSelectedRelic() {
        return selectedRelic;
    }

    /*
    public void getVer(){
        final MutableLiveData<VersionResult> result = new MutableLiveData<VersionResult>();
        result.postValue(new VersionResult());
        api.getVersion().enqueue(
                new Callback<VersionResult>(){
                    @Override
                    public void onResponse(Call <VersionResult> call,Response<VersionResult> response){
                        Log.d("MWEIN","all good version check");
                        //setVersion(response.body().vernum);
                        setVersion(4);
                    }

                    @Override
                    public void onFailure(Call <VersionResult> call,Throwable t){
                        Log.d("MWEIN","version check failed");
                        Log.d("MWEIN",t.toString());
                    }
                });
    }
*/


    public long insertRelic(Relic newRelic) {
        Future<Long> flong = databaseWriteExecutor.submit(() -> {
            return ceriMuseumDao.insert(newRelic);
        });
        long res = -1;
        try {
            res = flong.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedRelic.setValue(newRelic);
        return res;
    }

    public int updateRelic(Relic relic) {
        Future<Integer> fint = databaseWriteExecutor.submit(() -> {
            return ceriMuseumDao.update(relic);
        });
        int res = -1;
        try {
            res = fint.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedRelic.setValue(relic);
        return res;
    }

    public void deleteRelic(String id) {
        databaseWriteExecutor.execute(() -> {
            ceriMuseumDao.deleteRelic(id);
        });
    }

    public void getRelic(String id)  {
        Future<Relic> fcity = databaseWriteExecutor.submit(() -> {
            Log.d(TAG,"selected id="+id);
            return ceriMuseumDao.getRelicById(id);
        });
        try {
            selectedRelic.setValue(fcity.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void loadRelic(Relic relic){
        isLoading.postValue(Boolean.TRUE);
        api.getDBRelic(relic.getId()).enqueue(
                new Callback<RelicResponse>() {
                    @Override
                    public void onResponse(Call<RelicResponse> call, Response<RelicResponse> response) {
                        RelicResult.transferInfo(relic.getId(), response.body(), relic);
                        updateRelic(relic);
                        if(nbAPIloads==0)
                            isLoading.postValue(Boolean.FALSE);
                        else
                            nbAPIloads-=1;
                        Log.d("API",response.toString());

                        if (nbAPIloads<0)
                            nbAPIloads=0;
                    }

                    @Override
                    public void onFailure(Call<RelicResponse> call, Throwable t) {
                        Log.d("Failure",t.getMessage());
                        if(nbAPIloads==0)
                            isLoading.postValue(Boolean.FALSE);
                        else
                            nbAPIloads-=1;
                        webServiceThrowable.postValue(t);

                        if (nbAPIloads<0)
                            nbAPIloads=0;
                    }

                }

        );

    }

    public void loadAllRelics(){
        //List<Relic> listRelics = ceriMuseumDao.getSynchrAllRelics();

        isLoading.postValue(true);
        Relic c = new Relic("",0,"");
        final MutableLiveData<RelicResult> result = new MutableLiveData<RelicResult>();
        result.postValue(new RelicResult(true, null));
        api.getListRelic().enqueue(
                new Callback<Map<String,RelicResponse>>() {
                    @Override
                    public void onResponse(Call<Map<String,RelicResponse>> call,
                                           Response<Map<String,RelicResponse>> response) {
                        //result.postValue(new WeatherResult(false,null));
                        if (response.body() == null){
                        }else {
                            for (Map.Entry<String, RelicResponse> entry : response.body().entrySet()) {
                                String k = entry.getKey();
                                RelicResponse v = entry.getValue();
                                c.setId(k);
                                RelicResponse.transferInfo(v, c);
                                insertRelic(c);
                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<Map<String,RelicResponse>>call, Throwable t) {
                        Log.d("MWEIN", t.toString());
                        Log.d("MWEIN","failed to update");
                        //result.postValue(new MuseumResult(false, null));
                    }
                });

        isLoading.postValue(false);


    }

    public MutableLiveData<Boolean> getIsLoading() {
        return this.isLoading;
    }
    public MutableLiveData<Throwable> getWebServiceThrowable() {
        return this.webServiceThrowable;
    }
}
