package fr.uavignon.ceri.tp3.data;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HashMapTypeConverter {
    @TypeConverter
    public static Map<String,String> stringToSomeObjectList(String data) {
        Gson gson = new Gson();
        if (data == null) {
            return null;
        }

        Type listType = new TypeToken<Map<String,String>>() {}.getType();

        return gson.fromJson(data, listType);
    }

    @TypeConverter
    public static String someObjectListToString(Map<String,String> someObjects) {
        Gson gson = new Gson();
        return gson.toJson(someObjects);
    }
}
