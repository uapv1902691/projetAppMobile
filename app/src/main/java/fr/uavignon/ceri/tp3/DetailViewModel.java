package fr.uavignon.ceri.tp3;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import fr.uavignon.ceri.tp3.data.Relic;
import fr.uavignon.ceri.tp3.data.RelicRepository;

public class DetailViewModel extends AndroidViewModel {
    public static final String TAG = DetailViewModel.class.getSimpleName();

    private RelicRepository repository;
    private MutableLiveData<Relic> relic;

    public DetailViewModel (Application application) {
        super(application);
        repository = RelicRepository.get(application);
        relic = new MutableLiveData<>();
    }

    public void setRelic(String id) {
        repository.getRelic(id);
        relic = repository.getSelectedRelic();
    }
    LiveData<Relic> getRelic() {
        return relic;
    }

    public void updateRelic(){
        repository.loadRelic(relic.getValue());
    }

    public LiveData<Boolean> getIsLoading() {
        return repository.getIsLoading();
    }

    public MutableLiveData<Throwable> getWebServiceThrowable() {
        return repository.getWebServiceThrowable();
    }

}

