package fr.uavignon.ceri.tp3.data;

import java.util.HashMap;
import java.util.List;

public class RelicResponse {

        public static void transferInfo(RelicResponse v, Relic c) {
                if (v.year != null) {
                        c.setYear(v.year);
                }
                if (v.description != null) {
                        c.setDescription(v.description);
                }
                if (v.name != null) {
                        c.setName(v.name);
                }
                if (v.brand != null) {
                        c.setBrand(v.brand);
                }
                if (v.working != null) {
                        c.setWorking(v.working);
                } else {
                        c.setWorking(false);
                }
                if (v.timeFrame != null) {
                        c.setTimeFrame(v.timeFrame);
                }
                if (v.technicalDetails != null) {
                        c.setTechnicalDetails(v.technicalDetails);
                }
                if (v.categories != null) {
                        c.setCategories(v.categories);
                }
                if (v.pictures != null) {
                        c.setPictures(v.pictures);
                }

        }
        public final HashMap<String,String> pictures=null;
        public final Integer year=null;
        public final List<String> timeFrame=null;
        public final String name = null;
        public final String brand = null;
        public final String description = null;
        public final List<String> technicalDetails=null;
        public final List<String> categories=null;
        public final Boolean working=null;

}

