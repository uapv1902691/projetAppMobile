package fr.uavignon.ceri.tp3;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;
import java.util.concurrent.CountDownLatch;

import fr.uavignon.ceri.tp3.data.Relic;
import fr.uavignon.ceri.tp3.data.RelicRepository;

public class ListViewModel extends AndroidViewModel {
    private RelicRepository repository;
    private LiveData<List<Relic>> allRelics;

    private MutableLiveData<Boolean> isLoading;
    private MutableLiveData<Throwable> webServiceThrowable;

    public ListViewModel (Application application) {
        super(application);
        repository = RelicRepository.get(application);
        allRelics = repository.getAllRelics();
        isLoading = new MutableLiveData<>();
        isLoading.postValue(Boolean.FALSE);
        webServiceThrowable = new MutableLiveData<>();
    }

    public void RestoreList(){
        CountDownLatch latch = new CountDownLatch(1);
        Thread t = new Thread(){
            public void run(){
                repository = RelicRepository.get(new Application());
                allRelics = repository.getAllRelics();
                latch.countDown();
            }
        };
        t.start();
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }



    LiveData<List<Relic>> getAllRelics() {
        return allRelics;
    }

    public void deleteRelic(String id) {
        repository.deleteRelic(id);
    }

    public void loadWeatherAllCities(){
        repository.loadAllRelics();
    }


    public MutableLiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    public MutableLiveData<Throwable> getWebServiceThrowable() {
        return webServiceThrowable;
    }
}
