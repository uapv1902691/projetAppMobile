package fr.uavignon.ceri.tp3.data.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import fr.uavignon.ceri.tp3.data.Relic;

@Dao
public interface CeriMuseumDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Relic relic);

    @Query("DELETE FROM relic_table")
    void deleteAll();

    @Query("SELECT * from relic_table ORDER BY name ASC")
    LiveData<List<Relic>> getAllRelics();

    @Query("SELECT * from relic_table ORDER BY name ASC")
    List<Relic> getSynchrAllRelics();

    @Query("DELETE FROM relic_table WHERE _id = :id")
    void deleteRelic(String id);

    @Query("SELECT * FROM relic_table WHERE _id = :id")
    Relic getRelicById(String id);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    int update(Relic relic);
}
